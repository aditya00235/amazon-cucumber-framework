package uiStore;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;



public class HomePageUI 
{
	public static By search_box=By.id("twotabsearchtextbox");
	public static By search_button=By.id("nav-search-submit-button");
	public static By logo=By.xpath("//a[@id='nav-logo-sprites']");
	public static By login_button=By.xpath("(//a[@data-nav-role='signin'])[1]");
	public static By navbar=By.xpath("//div[@id='nav-main']");
}
