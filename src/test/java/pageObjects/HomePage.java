package pageObjects;


import org.apache.log4j.Logger;
//import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import reusableObjects.ReusableMethods;
import uiStore.HomePageUI;



public class HomePage 
{
		public static boolean search(WebDriver driver,String searchitem, Logger log) throws Exception
		{
			if(ReusableMethods.getElements(HomePageUI.search_box, driver))
			{
				log.info("Search box found");
				if(ReusableMethods.sendKeys(HomePageUI.search_box, searchitem, driver))
				{	
					log.info("Keys send in the Search box ");
					log.error("This is dummy error which i am giving after keys send in search box");
					
					if(ReusableMethods.click(HomePageUI.search_button, driver))
					{
						log.info("Search button clicked");
						return true;
					}
				}
			}
			return false;
		}
		
		
		public static boolean validateNavBar(WebDriver driver,Logger log)
		{
			if(ReusableMethods.getElements(HomePageUI.navbar, driver))
			{
				log.warn("Navbar displayed");
				return true;
			}
			return false;
		}
}
