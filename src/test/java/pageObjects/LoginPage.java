package pageObjects;

import org.apache.log4j.Logger;
//import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import reusableObjects.ReusableMethods;
import uiStore.HomePageUI;
import uiStore.LoginUI;



public class LoginPage
{

	public static boolean login_function(WebDriver driver,String username,Logger log) throws Exception
	{
		if(ReusableMethods.getElements(HomePageUI.login_button, driver))
		{
			log.info("Login section found");
			if(ReusableMethods.click(HomePageUI.login_button, driver))
			{
				log.info("Login section clicked");
				ReusableMethods.getElements(LoginUI.email_box, driver);
				log.info("Email Box found");
				ReusableMethods.sendKeys(LoginUI.email_box, username, driver);
				log.info("Keys send in e mail box or username");
				ReusableMethods.click(LoginUI.continue_button, driver);
				log.info("continue button clicked");
				return true;
			}
		}
		return false;
	}
}
