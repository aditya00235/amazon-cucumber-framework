package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.junit.Cucumber;
import reusableObjects.Base;
import reusableObjects.GenerateProp;
import uiStore.HomePageUI;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

//@RunWith(Cucumber.class)
public class SearchItems {
	WebDriver driver;
	Properties prop;
	Logger log=Logger.getLogger(SearchItems.class);

    @Given("^User opens the browser$")
    public void user_opens_the_browser() throws Throwable {
    	driver=Base.initializeDriver();
        log.info("Opens the browser");
    }

    @Given("^Navigate to the URL$")
    public void navigate_to_the_url() throws Throwable {
    	 prop=GenerateProp.generatepropobject();
         driver.get(prop.getProperty("url"));
         log.info("Url is invoked by the browser");
         driver.manage().window().maximize();
    }
    
    @When("^User enters the item in searchBox with \"([^\"]*)\"$")
    public void user_enters_the_item_in_searchBox_with(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    	driver.findElement(HomePageUI.search_box).sendKeys(arg1);
        log.info("Search box found and keys send to it");
    }

    @When("^click on search button$")
    public void click_on_search_button() throws Throwable {
    	driver.findElement(HomePageUI.search_button).click();
    	log.info("Search button clicked");
    }

    @Then("^It shows different product$")
    public void it_shows_different_product() throws Throwable {
    	Assert.assertTrue(true);
    }

    @And("^closes browser$")
    public void closes_browser() throws Throwable {
    	driver.close();
        driver=null;
    }

}