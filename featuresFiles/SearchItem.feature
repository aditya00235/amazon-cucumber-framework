Feature: Search items in Amazon

Scenario Outline:As a user I want to search different items in Amazon
Given User opens the browser
Given Navigate to the URL
When User enters the item in searchBox with "<items>" 
When click on search button
Then It shows different product
And closes browser

Examples:
|items			|
|Samsung mobile	|
|Mi TV			|